$(document).ready(function () {
                // Custom function which toggles between sticky class (is-sticky)
                var stickyToggle = function (sticky, stickyWrapper, scrollElement) {
                    var stickyHeight = sticky.outerHeight();
                    var stickyTop = stickyWrapper.offset().top;
                    if (scrollElement.scrollTop() >= stickyTop) {
                        stickyWrapper.height(stickyHeight);
                        sticky.addClass("is-sticky");
                    }
                    else {
                        sticky.removeClass("is-sticky");
                        stickyWrapper.height('auto');
                    }
                };

                // Find all data-toggle="sticky-onscroll" elements
                $('[data-toggle="sticky-onscroll"]').each(function () {
                    var sticky = $(this);
                    var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
                    sticky.before(stickyWrapper);
                    sticky.addClass('sticky');

                    // Scroll & resize events
                    $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function () {
                        stickyToggle(sticky, stickyWrapper, $(this));
                    });

                    // On page load
                    stickyToggle(sticky, stickyWrapper, $(window));
                });
				
				var date=new Date().getFullYear();
				//date
				$('#date').text(date);
				
				//owl carousel
				$("#index-slider").owlCarousel({
 
					  navigation : true, // Show next and prev buttons
					  slideSpeed : 300,
					  paginationSpeed : 400,
					  singleItem:true,
					  items : 1,
					  
					  loop: true,
					  nav: false,
					  dots: true,
					  autoplay: false,
					  autoplayTimeout: 2500,
					  
					  
					  // "singleItem:true" is a shortcut for:
					  // items : 1, 
					  // itemsDesktop : false,
					  // itemsDesktopSmall : false,
					  // itemsTablet: false,
					  // itemsMobile : false
				 
				  });
			
					
					$("#about-slider").owlCarousel({
 
					  navigation : true, // Show next and prev buttons
					  slideSpeed : 300,
					  paginationSpeed : 400,
					  
					  items :4,
					  
					  loop: true,
					  nav: true,
					  dots: true,
					  autoplay: false,
					  autoplayTimeout: 2500,
					  responsiveClass:true,
					responsive:{
						0:{
							items:1,
							nav:true
						},
						600:{
							items:3,
							nav:false
						},
						1000:{
							items:4,
							nav:true,
							loop:false
						}
					}
					  
					  
				 
				  });
					
					
					 //$("#bigImage").mouseover(function(){
						//$(".hover").css("background-color", "yellow");
					//});
				  //$("#bigImage").mouseout(function(){
					//$("hover").css("background-color", "lightgray");
				  //});
			 
			     
					$(".icon-title , .icon-service").mouseover(function () {
						$(this).find('img').attr('src', $(this).find('img').attr("data-hover"));
					}).mouseout(function () {
						$(this).find('img').attr('src', $(this).find('img').attr("data-src"));
					})
					
					//$("#bigImage").mouseout(function () {
						//$(this).attr('src', $(this).attr("data-src"));
					//});
			    
			
			});
			
			
				function toggleResetPswd(e){
						e.preventDefault();
						$('#logreg-forms .form-signin').toggle() // display:block or none
						$('#logreg-forms .form-reset').toggle() // display:block or none
					}

					function toggleSignUp(e){
						e.preventDefault();
						$('#logreg-forms .form-signin').toggle(); // display:block or none
						$('#logreg-forms .form-signup').toggle(); // display:block or none
					}

					$(()=>{
						// Login Register Form
						$('#logreg-forms #forgot_pswd').click(toggleResetPswd);
						$('#logreg-forms #cancel_reset').click(toggleResetPswd);
						$('#logreg-forms #btn-signup').click(toggleSignUp);
						$('#logreg-forms #cancel_signup').click(toggleSignUp);
				})
			    
				
			
			
			