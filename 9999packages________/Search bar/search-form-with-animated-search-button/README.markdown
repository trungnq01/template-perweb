# Search Form With Animated Search Button
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/himalayasingh/pen/dqjLgO](https://codepen.io/himalayasingh/pen/dqjLgO).

 Search form with animated search button which transforms into right arrow on hover (the search glass icon changes to right arrow).