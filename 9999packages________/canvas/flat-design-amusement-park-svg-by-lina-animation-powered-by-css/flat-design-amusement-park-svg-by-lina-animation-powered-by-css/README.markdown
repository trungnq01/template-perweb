# Flat design amusement park svg by Lina (animation powered by CSS) 
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/gxash/pen/YqmxWg](https://codepen.io/gxash/pen/YqmxWg).

 Here's an svg from my lovely Lina - her flat design amusement park for debut dribbble shot, animated by me with power of pure css3, made here at <a href="http://www.zajno.com" rel="nofollow noreferrer">Zajno</a>

https://dribbble.com/shots/2747921-Flat-Vector-Ferris-Wheel-Animation

